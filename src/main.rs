// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

extern crate serde_derive;

use shared_mem_queue::SharedMemQueue;

use std::fs;
use std::io::stdin;
use std::num::ParseIntError;
use std::path::PathBuf;
use std::process::exit;
use std::sync::mpsc;
use std::thread;

use std::sync::atomic::{AtomicBool, Ordering};

use try_ascii::try_ascii;

use core::fmt::{Display, Formatter};

use serde_derive::Deserialize;

#[derive(Debug, Clone)]
pub struct SharedMemChannel {
    pub origin: usize,
    pub length: usize,
}

#[derive(Debug, Clone, Deserialize)]
pub struct SharedMemChannelToml {
    pub origin: String,
    pub length: String,
}

impl SharedMemChannel {
    /// parses data read from file into usable data\
    /// converts hex to usize
    pub fn from_toml(toml: SharedMemChannelToml) -> Result<Self, ParseIntError> {
        let origin = parse_to_usize(toml.origin)?;
        let length = parse_to_usize(toml.length)?;

        Ok(SharedMemChannel { origin, length })
    }
}

impl Display for SharedMemChannel {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "({:#X} / {:#X})", self.origin, self.length)
    }
}

#[derive(Debug, Clone, Deserialize)]
struct SharedMemoryToml {
    pub read_channel: Option<SharedMemChannelToml>,
    pub write_channel: Option<SharedMemChannelToml>,
}

#[derive(Debug, Clone)]
struct SharedMemory {
    pub read_channel: Option<SharedMemChannel>,
    pub write_channel: Option<SharedMemChannel>,
}

impl SharedMemory {
    /// parses data read from file into usable data\
    /// converts hex to usize
    pub fn from_toml(toml: SharedMemoryToml) -> Result<Self, ParseIntError> {
        let mut mem = SharedMemory {
            read_channel: None,
            write_channel: None,
        };
        if let Some(read_channel) = toml.read_channel {
            let read_channel = SharedMemChannel::from_toml(read_channel)?;
            mem.read_channel = Some(read_channel);
        };

        if let Some(write_channel) = toml.write_channel {
            let write_channel = SharedMemChannel::from_toml(write_channel)?;

            mem.write_channel = Some(write_channel);
        };
        Ok(mem)
    }
}

// TODO It seems that const_format does not understand {:#X} like the normal format! does but the memory addresses should be displayed in hex
const HELP_STR: &str = "
    Opens a bidirectional shared-memory-channel (i.e. two unidirectional channels) according to the given parameters.

    If one of the four parameters (--read_origin, --read_length, --write_origin, --write_length) is missing, the configuration file is tried afterwards. If --file is specified, it is read, otherwise ./memcom.toml is tried as default.

    Currently, unidirectional communication is only supported via the config file because all four command-line parameters are checked collectively.
    
        --read_origin (string) read channel origin, specified as decimal or as hex with \"0x\" prefix
        --read_length (string) read channel length
        --write_origin (string) write channel origin
        --write_length (string) write channel length
        -f, --file (path) Config file path
        --output (default 'stdout') Specifies the output device/file, NOT YET IMPLEMENTED
        -h, --help Shows this text

    The config file has to be a toml file with the following example syntax:

        [read_channel]
        origin = \"0x10048000\"
        length = \"0x2000\"
        [write_channel]
        origin = \"0x1004a000\"
        length = \"0x2000\"";

// Just a bool Flag set in SIG_INT / CTRL+C Handler to terminate the application safely
static SIG_INT: AtomicBool = AtomicBool::new(false);

/// Parses String to usize
fn parse_to_usize(string: String) -> Result<usize, ParseIntError> {
    if string.starts_with("0x") {
        let string = string.replacen("0x", "", 1);
        let string = string.as_str();
        usize::from_str_radix(string, 16)
    } else {
        string.parse::<usize>()
    }
}

/// load mem_profile from file_path
fn load_file(file_path: PathBuf) -> Result<SharedMemory, ParseIntError> {
    let file_content = match fs::read_to_string(file_path.clone()) {
        Ok(c) => c,
        Err(_) => {
            eprintln!("Error while reading file {}!", file_path.display());
            eprintln!("Missing Channel configuration. See help for more details.");
            exit(1);
        }
    };
    let mem_profile: SharedMemoryToml = match toml::from_str(&file_content) {
        Ok(c) => c,
        Err(_) => {
            eprintln!("Error loading data from file: {}!", file_path.display());
            exit(1);
        }
    };
    SharedMemory::from_toml(mem_profile)
}

fn main() {
    // Set ctrl+c/SIGINT Handler to terminate the application
    ctrlc::set_handler(move || {
        println!();
        println!("Interrupt received");

        SIG_INT.store(true, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    // Construct the args from help text
    let args = lapp::parse_args(HELP_STR);

    if args.get_bool("help") {
        println!("{}", HELP_STR);
    } else if let (Ok(read_origin), Ok(write_origin), Ok(read_length), Ok(write_length)) = (
        args.get_string_result("read_origin"),
        args.get_string_result("write_origin"),
        args.get_string_result("read_length"),
        args.get_string_result("write_length"),
    ) {
        let read_origin_usize = parse_to_usize(read_origin);
        let write_origin_usize = parse_to_usize(write_origin);
        let read_length_usize = parse_to_usize(read_length);
        let write_length_usize = parse_to_usize(write_length);

        if let (Ok(read_origin), Ok(write_origin), Ok(read_length), Ok(write_length)) = (
            read_origin_usize.clone(),
            write_origin_usize.clone(),
            read_length_usize.clone(),
            write_length_usize.clone(),
        ) {
            println!("Found CL args");
            let mem_profile = SharedMemory {
                read_channel: Some(SharedMemChannel {
                    origin: read_origin,
                    length: read_length,
                }),
                write_channel: Some(SharedMemChannel {
                    origin: write_origin,
                    length: write_length,
                }),
            };

            operate_channels(&mem_profile);
        } else {
            if let Err(read_channel_err) = read_origin_usize {
                eprintln!("Got an error while parsing origin of read channel");
                eprintln!("{}", read_channel_err);
            }
            if let Err(write_channel_err) = write_origin_usize {
                eprintln!("Got an error while parsing origin of write channel");
                eprintln!("{}", write_channel_err);
            }
            if let Err(read_length_err) = read_length_usize {
                eprintln!("Got an error while parsing length of read channel");
                eprintln!("{}", read_length_err);
            }
            if let Err(write_length_err) = write_length_usize {
                eprintln!("Got an error while parsing length of write channel");
                eprintln!("{}", write_length_err);
            }
            exit(1);
        }
    } else if let Ok(file_path) = args.get_path_result("file") {
        println!("Found File path arg");
        let mem_profile = load_file(file_path);
        if let Ok(mem_profile) = mem_profile {
            operate_channels(&mem_profile);
        } else if let Err(mem_err) = mem_profile {
            eprintln!("Error: {}", mem_err);
        };
    } else {
        println!("No channel configuration specified on command-line, trying to read default file: ./memcom.toml");
        let file_path = PathBuf::from("./memcom.toml");

        let mem_profile = load_file(file_path);
        if let Ok(mem_profile) = mem_profile {
            operate_channels(&mem_profile);
        } else if let Err(mem_err) = mem_profile {
            eprintln!("Error: {}", mem_err);
        };
    }

    println!("Terminating the Application");
}

fn operate_channels(mem_profile: &SharedMemory) {
    let dev_mem = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .open("/dev/mem")
        .expect("Could not open /dev/mem, do you have root privileges?");

    println!("Now Operate Channel:");
    println!("{:?}", mem_profile);

    let mut mmap_m4a7;
    let mut mmap_a7m4;

    // For echoing stdin
    let (tx_read_stdin, rx_read_stdin) = mpsc::channel();

    // We only need to read stdin if we also have a write channel
    if mem_profile.write_channel.is_some() {
        thread::spawn(move || loop {
            let mut buf = String::new();

            if SIG_INT.load(Ordering::SeqCst) {
                break;
            }

            if stdin().read_line(&mut buf).is_ok() {
                tx_read_stdin.send(buf).unwrap();
            }
        });
    }

    let mut read_channel = if let Some(read_channel) = &mem_profile.read_channel {
        mmap_m4a7 = unsafe {
            memmap::MmapOptions::new()
                .len(read_channel.length)
                .offset(read_channel.origin as u64)
                .map_mut(&dev_mem)
                .unwrap()
        };

        Some(unsafe { SharedMemQueue::attach(mmap_m4a7.as_mut_ptr(), read_channel.length) })
    } else {
        None
    };

    let mut write_channel = if let Some(write_channel) = &mem_profile.write_channel {
        mmap_a7m4 = unsafe {
            memmap::MmapOptions::new()
                .len(write_channel.length)
                .offset(write_channel.origin as u64)
                .map_mut(&dev_mem)
                .unwrap()
        };

        Some(unsafe { SharedMemQueue::attach(mmap_a7m4.as_mut_ptr(), write_channel.length) })
    } else {
        None
    };

    loop {
        if SIG_INT.load(Ordering::SeqCst) {
            break;
        }

        if let Some(read_channel) = &mut read_channel {
            if read_channel.size() > 0 {
                while read_channel.size() > 0 {
                    let mut buf = [0u8; 1];
                    read_channel.blocking_read(&mut buf);

                    print!("{:?}", try_ascii(&buf));
                }
            }
        }

        if let Some(write_channel) = &mut write_channel {
            if let Ok(s) = rx_read_stdin.try_recv() {
                write_channel.blocking_write(s.as_bytes());
            }
        };
    }

    println!("Closing Channels");
    // TODO Close Channels
}
