# Changelog

## v0.1.0
- First public release on crates.io
- Configuration via command-line arguments or toml file
- Simple writing to and reading from a single bidirectional channel, unidirectional support if configured via toml file
