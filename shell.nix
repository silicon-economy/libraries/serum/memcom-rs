{ nightly ? false }:
let
  # Oxalica's Rust overlay gives us the rust-bin function which allows us
  # to select a specific Rust toolchain or to switch between stable and nightly
  rust_overlay = import
    (builtins.fetchTarball {
      name = "rust-overlay-2024-01-15";
      url = "https://github.com/oxalica/rust-overlay/archive/d681ac8a92a1cce066df1d3a5a7f7c909688f4be.tar.gz";
      sha256 = "1mrcbw14dlch89hnncs7pnm7lh4isqx7xywha6i97d8xs24spfvv";
    });

  # Our own overlay with additional build environment tools
  serum_overlay = import (builtins.fetchGit {
    name = "serum-overlay-2023-05-05";
    url = "https://git.openlogisticsfoundation.org/silicon-economy/libraries/serum/serum-nix-overlay.git";
    allRefs = true;
    rev = "d3781d433a1ca275c3882470dfb9b7eaeec9c0d6"; # Use the newest version here
  });

  # Pinned nixpkgs
  nixpkgsBase = import (builtins.fetchTarball {
    name = "nixpkgs-stable-23.11";
    url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/23.11.tar.gz";
    # sha256 = "0000000000000000000000000000000000000000000000000000";
    sha256 = "1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
  });

  # nixpkgs configured for cross-compilation to stm32mp1 linux systems
  pkgs = nixpkgsBase {
    overlays = [ serum_overlay rust_overlay ];
    crossSystem = {
      config = "armv7l-unknown-linux-gnueabihf";
    };
  };

in
pkgs.callPackage ({stdenv, buildPackages, gitlab-clippy, cargo2junit, cargo-udeps, cargo-deny, cargo-tarpaulin, nodePackages, git, cacert, which, rust-bin}:
  stdenv.mkDerivation {
    name = "rust-env";

    depsBuildBuild = [
      # Native C compiler, see https://nixos.org/manual/nixpkgs/stable/#ssec-cross-cookbook
      buildPackages.stdenv.cc
      # CI/CD tools
      gitlab-clippy
      cargo2junit
      cargo-udeps
      cargo-deny
      cargo-tarpaulin
      nodePackages.cspell
      git
      cacert
      # Native buildInputs to run tests on the build host
      
      # Nix debugging tools
      which
    ];

    nativeBuildInputs = [
      # Choose between a stable or nightly Rust version
      (if nightly
        then rust-bin.nightly."2024-01-15"
        else rust-bin.stable."1.75.0"
        ).default
    ];

    buildInputs = [
    ];

    # Set Environment Variables
    RUST_BACKTRACE = 1;

    # Specify the linker to use for cross compilation. With this environment
    # variable set, we do not need to specify it in .cargo/config.toml anymore.
    CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER = "${stdenv.cc.targetPrefix}gcc";

    shellHook = ''
      MEMCOM_NATIVE_TRIPLE=$(rustc -vV | awk '/host/ { print $2 }');
    '';
  })
  {}
