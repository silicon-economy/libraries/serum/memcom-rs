# memcom

Memcom is a command line tool for interacting with the shared memory between the M4 and A7
processors of the ST32MP1XX controllers. It is based on the
[`shared-mem-queue`](https://crates.io/crates/shared-mem-queue) crate so it can be used to interact
with applications which use that crate, too, or follow the protocol. For example, it can be used to
write commands to or read logs from the M4 processor.

## Features

The tool can operate a read channel (M4 to A7) and a write channel (A7 to M4) simultaneously. The
channel origin and length can either be specified via the command-line arguments
`--read_origin`, `--read_length`, `--write_origin`, `--write_length` or via a config file which is
specified via the command-line flag `--file` (`-f`). As a fallback, `./memcom.toml` is tried as
default configuration file.

The config file has to be a toml file with the following syntax:
```toml
[read_channel]
origin = "0x10048000"
length = "0x2000"
[write_channel]
origin = "0x1004a000"
length = "0x2000"
```

## Example
```
memcom --read_origin 0x10048000 --read_length 0x2000 --write_origin 0x1004a000 --write_length 0x2000
```

## License

Open Logistics Foundation License
Version 1.3, January 2023

See the LICENSE file in the top directory.

## Contact

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
